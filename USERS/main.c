
#include <stm32f10x.h>
#include <stdio.h>
#include <string.h>

#include "systick.h"
#include "usart1.h"
#include "usart2.h"
#include "usart2printf.h"
#include "client.h"


extern unsigned char CmdRx_Buffer[20];



int main()
{
	bluetooth_init(115200);
	ESP8266_init(9600);
	

	printf("\r\n  ESP8266 TEST   \r\n");
	
	//接收到信息之后，WiFi给服务器发送信息“厕纸即将用完”
	ESP8266_client_link_server();
	
	//一直监测蓝牙的信息
	//strstr返回后面字符串在前面字符串中的第一次位置
	while(1)
	{
		while(strstr((const char *)CmdRx_Buffer,"LEDON") != NULL)
		{
			memset(CmdRx_Buffer,0,sizeof(CmdRx_Buffer));		//数组清零
			
			printf ( "\r\nreceive...\r\n" );
	
			ESP8266_SendString ( ENABLE, "\r\n厕纸即将用完！！\r\n", 0, Single_ID_0 );   //发送数据
		}
				
	}
}



